const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);



app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    console.log('Un utilisateur est connecté');

    socket.on('connect', (co) => {
        io.emit('connected', co);
    })
    socket.on('disconnect', (deco) => {
        console.log("Un utilisateur s'est déconnecté");
        io.emit("disconnected", deco);
    });
    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
    });
});

http.listen(3000, () => {
    console.log('Server Ready');
});